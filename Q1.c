#include<stdio.h>

void pattern(int num);

int main()
{
	int num;
    //get the user input for the number of rows//
	printf("Enter the Number of Rows in the Triangle : ");
	scanf("%d", &num);

	pattern(num);

	return 0;
}
void pattern(int num)
{
	for(int a = 1 ; a <= num ; a++)
	{
		for(int j = a ; j >= 1 ; j--)
		{
			printf("%d",j);
		}
		printf("\n");
	}
}
